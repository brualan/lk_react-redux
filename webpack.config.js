const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyjsPlugin = require('uglifyjs-webpack-plugin');

const { NODE_ENV = 'development', PORT = 8080 } = process.env;
const srcDir = path.join(__dirname, 'src');
const publicDir = path.join(__dirname, 'public');

module.exports = {
  context: srcDir,
  entry: {
    app: NODE_ENV !== 'development' ? srcDir : [
      `webpack-dev-server/client?http://localhost:${PORT}`,
      'webpack/hot/only-dev-server',
      srcDir
    ]
  },
  output: {
    path: publicDir,
    filename: '[name].[hash:6].js',
    chunkFilename: '[id].[hash:6].js',
    publicPath: process.env.NODE_ENV === 'production'
      ? '/react/'
      : '/'
  },
  watch: NODE_ENV === 'development',
  watchOptions: {
    ignored: /node_modules/
  },
  devtool: NODE_ENV !== 'production' ? 'cheap-module-source-map' : false,
  resolve: {
    modules: [path.join(__dirname, 'node_modules'), srcDir],
    extensions: ['.js', '.jsx']
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        enforce: 'pre',
        loader: 'eslint-loader',
        include: [srcDir]
      },
      {
        test: /\.jsx?$/,
        include: [srcDir],
        loader: 'babel-loader'
      },
      {
        test: /\.(png|jpg|jpeg|webp)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[hash:6].[ext]',
          outputPath: 'images/'
        }
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: false
            }
          }
        ]
      },
      {
        test: /\.sass$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              import: true,
              modules: false
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                require('autoprefixer')(),
                require('lost')()
              ]
            }
          },
          {
            loader: 'sass-loader',
            options: {
              includePaths: [
                srcDir,
                path.join(__dirname, 'node_modules')
              ]
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      process: {
        env: {
          NODE_ENV: JSON.stringify(NODE_ENV)
        }
      }
    }),
    new HtmlWebpackPlugin({
      template: path.join(srcDir, 'index.html')
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  ],
  devServer: {
    historyApiFallback: true,
    overlay: true,
    port: PORT,
    hot: true
  }
};

if (NODE_ENV === 'production') {
  module.exports.plugins.push(new UglifyjsPlugin({
    test: /.jsx?$/,
    parallel: true,
    cache: true
  }));
}
