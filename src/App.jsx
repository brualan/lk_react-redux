import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import PrivateRoute from 'core/components/PrivateRoute';
import { Header } from 'core/components/Header';
import { AlertsContainer } from 'features/alerts/components';

import { Auth, Dashboard, Cameras } from 'routes';

/* For <Provider> we need class component */
/* eslint-disable react/prefer-stateless-function */

class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <Switch>
          <PrivateRoute exact path="/" component={Dashboard} />
          <PrivateRoute path="/cameras" component={Cameras} />
          <Route exact path="/auth" component={Auth} />
          <Redirect to="/" />
        </Switch>
        <AlertsContainer />
        {process.env.NODE_ENV !== 'production' && (
          <a href="https://parking.goodline.info/api/user/delete?phone=79521695478">
            Удалить номер
          </a>
        )}
      </React.Fragment>
    );
  }
}

export default App;
