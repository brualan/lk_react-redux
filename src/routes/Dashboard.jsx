import React, { Fragment } from 'react';
import { Container } from 'core/components/Container';

import CamerasContainer from 'features/cameras/components/CamerasContainer';
import ViewersList from 'features/cameras/components/ViewersList';
import { TelegramJumbotron } from 'features/auth/components/Jumbotron';
import './Dashboard.sass';

// implements a panel with cameras and viewers
export const Dashboard = () => (
  <Fragment>
    <TelegramJumbotron />
    <div className="dashboard">
      <Container className="dashboard__container">
        <div className="dashboard__col">
          <CamerasContainer />
        </div>
        <div className="dashboard__col">
          <ViewersList />
        </div>
      </Container>
    </div>
  </Fragment>
);

