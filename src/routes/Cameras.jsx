import React from 'react';
import { Container } from 'core/components/Container';
import { Gutter } from 'core/components/Gutter';
import { Switch, Route, Redirect } from 'react-router-dom';
import CameraFormContainer from 'features/cameras/components/CameraFormContainer';
import CameraImageContainer from 'features/cameras/components/CameraImageContainer';
import CameraManufacturerContainer from 'features/cameras/components/CameraManufacturerContainer';
import ViewerFormContainer from 'features/cameras/components/ViewerFormContainer';

export const Cameras = () => (
  <Container>
    <Gutter size={1.2} />
    <Switch>
      <Route exact path="/cameras/:camera_id/manufacturer/:id?" component={CameraManufacturerContainer} />
      <Route exact path="/cameras/:id/image" component={CameraImageContainer} />
      <Route exact path="/cameras/:id/viewer" component={ViewerFormContainer} />
      <Route exact path="/cameras/new" component={CameraFormContainer} />
      <Redirect to="/" replace />
    </Switch>
  </Container>
);
