import React, { Fragment } from 'react';
import { Gutter } from 'core/components/Gutter';
import { SignUpFormContainer } from 'features/auth';
import { AuthJumbotron } from 'features/auth/components/Jumbotron';

export const Auth = () => (
  <Fragment>
    <AuthJumbotron />
    <Gutter size={1.5} />
    <SignUpFormContainer />
  </Fragment>
);
