import React from 'react';
import cn from 'classnames';
import './Title.sass';

export const Title = ({ className, level = 'h1', ...rest }) => {
  const classes = cn(
    'title', className, {
      [`title_level_${level}`]: true
    }
  );

  return (
    <div {...rest} className={classes} />
  );
};
