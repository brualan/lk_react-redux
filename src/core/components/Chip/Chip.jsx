import React from 'react';
import './Chip.sass';

export const Chip = ({ as: Component = 'div', icon: Icon, children, ...rest }) => (
  <Component {...rest} className={`chip ${rest.className || ''}`}>
    <Icon className="chip__icon" />
    <span className="chip__body">
      {children}
    </span>
  </Component>
);
