import React from 'react';
import './Container.sass';

export const Container = props => (
  <div {...props} className={`container ${props.className || ''}`} />
);
