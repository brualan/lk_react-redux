import React from 'react';
import cn from 'classnames';
import './Button.sass';

export const Button = ({ as: Component = 'button', skin = 'primary', ...rest }) => {
  const classes = cn('button', rest.className, {
    [`button_skin_${skin}`]: true
  });

  return (
    <Component {...rest} className={classes} />
  );
};
