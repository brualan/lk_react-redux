import React from 'react';
import cn from 'classnames';
import './Card.sass';

export const Card = props => (
  <div {...props} className={cn('card', props.className)} />
);

const Title = props => (
  <div {...props} className="card__title" />
);

const Body = props => (
  <div {...props} className="card__body" />
);

const Note = ({ status, ...props }) => (
  <div {...props} className={cn('card__note', { [`card__note_${status}`]: true })} />
);

Card.Title = Title;
Card.Body = Body;
Card.Note = Note;
