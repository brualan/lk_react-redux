import React from 'react';
import cn from 'classnames';
import './Spinner.sass';

export const Spinner = ({ absolute = true, overlay = false, withOverlay = false }) => {
  const classes = cn(
    'spinner', {
      spinner_absolute: absolute,
      spinner_overlay: overlay,
      spinner_with_overlay: withOverlay
    }
  );

  return (
    <div className={classes}>
      <div className="spinner__wheel" />
    </div>
  );
};
