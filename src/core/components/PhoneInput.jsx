import React from 'react';
import MaskedInput from 'react-maskedinput';
import { TextInput } from 'core/components/TextInput';

export const PhoneInput = props => (
  <TextInput
    as={MaskedInput}
    placeholder="+7 (900) 000-00-00"
    mask="+7 (111) 111-11-11"
    type="phone"
    {...props}
  />
);
