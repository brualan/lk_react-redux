import React from 'react';
import { Title } from 'core/components/Title';
import './Form.sass';

export const Form = props => (
  <form {...props} className={`form ${props.className || ''}`} />
);

Form.Title = props => (
  <Title {...props} className={`form__title ${props.className || ''}`} />
);

Form.Field = ({ className, label, children, ...rest }) => (
  <div {...rest} className={`form__field ${className || ''}`}>
    <div className="form__label">
      {label}
    </div>
    <div className="form__input">
      {children}
    </div>
  </div>
);

Form.Note = props => (
  <div {...props} className={`form__note ${props.className || ''}`} />
);
