import React from 'react';
import './Gutter.sass';

export const Gutter = ({ size, ...rest }) => (
  <div {...rest} style={{ marginTop: `${size * 24}px` }} />
);
