import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import * as selectors from 'features/auth/selectors';

const withStore = connect(state => ({
  isAuth: selectors.isAuth(state)
}));


const PrivateRoute = ({ component: Component, isAuth, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      isAuth
        ? <Component {...props} />
        : <Redirect to={{ pathname: '/auth', state: { from: props.location } }} />
    )}
  />
);

export default withStore(PrivateRoute);

