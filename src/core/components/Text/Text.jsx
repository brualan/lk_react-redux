import React from 'react';
import cn from 'classnames';
import './Text.sass';

export const Text = ({ className, size = 'md', ...rest }) => {
  const classes = cn(
    'text', className, {
      [`text_size_${size}`]: true
    }
  );

  return (
    <div {...rest} className={classes} />
  );
};
