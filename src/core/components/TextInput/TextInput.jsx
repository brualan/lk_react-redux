import React from 'react';
import cn from 'classnames';
import './TextInput.sass';

export const TextInput = ({ as: Component = 'input', ...rest }) => {
  const classes = cn(
    'text-input', rest.className
  );

  return (
    <Component {...rest} className={classes} />
  );
};
