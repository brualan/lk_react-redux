import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'core/components/Container';
import { logout } from 'features/auth/actions';
import { isAuth } from 'features/auth/selectors';
import './Header.sass';

const withStore = connect(
  state => ({ isAuth: isAuth(state) }), {
    onLogout: logout
  });

class Header extends React.Component {
  logout = (e) => {
    e.preventDefault();
    this.props.onLogout();
  };

  render() {
    return (
      <div className="header">
        <Container className="header__container">
          <a className="header__brand" href="/" target="_blank" rel="noopener noreferrer">
            Cервис поиска свободных парковочных мест
          </a>
          {this.props.isAuth && (
            <button className="header__auth-link" onMouseDown={this.logout}>
              Выход
            </button>
          )}
        </Container>
      </div>
    );
  }
}


export default withStore(Header);
