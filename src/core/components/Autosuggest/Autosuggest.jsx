import React from 'react';
import ReactAutosuggest from 'react-autosuggest';
import './Autosuggest.sass';

// const renderSuggestionsContainer = ({ containerProps, children }) => (
//   <div {...containerProps} className="autosuggest__container">
//     {children}
//   </div>
// );

const renderSuggestion = (suggestion, { isHighlighted }) => (
  <span className={`autosuggest__suggestion ${isHighlighted ? 'is-focused' : ''}`}>
    {suggestion.name}
  </span>
);

const theme = {
  container: 'autosuggest',
  containerOpen: 'is-open',
  input: 'autosuggest__input text-input',
  inputOpen: 'is-open',
  inputFocused: 'is-focused',
  suggestionsContainer: 'autosuggest__suggestions',
  suggestionsContainerOpen: 'is-open',
  suggestionsList: 'autosuggest__list',
  suggestion: 'autosuggest__suggestion-item',
  suggestionFirst: 'is-first',
  suggestionHighlighted: 'is-highlighted',
  sectionContainer: 'autosuggest__section-container',
  sectionContainerFirst: 'is-first',
  sectionTitle: 'autosuggest__section-title'
};

export const Autosuggest = props => (
  <ReactAutosuggest
    {...props}
    theme={theme}
    renderSuggestion={renderSuggestion}
  />
);
