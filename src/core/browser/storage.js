const storage = window.localStorage;

export const keys = {
  user: 'auth.user',
  token: 'auth.token',
  telegramGuideHidden: 'auth.teregram_guide_hidden'
};

export const saveUser = user => storage.setItem(keys.user, JSON.stringify(user));
export const getUser = () => {
  const user = storage.getItem(keys.user);
  return user !== null ? JSON.parse(user) : null;
};

export const saveToken = token => storage.setItem(keys.token, token);

export const getToken = () => storage.getItem(keys.token);

export const telegramGuideHidden = (value) => {
  if (value === undefined) {
    return storage.getItem(keys.telegramGuideHidden);
  }

  return storage.setItem(keys.telegramGuideHidden, value);
};

export const clean = () => {
  Object.keys(keys).forEach((key) => {
    storage.removeItem(keys[key]);
  });
};
