import axios from 'axios';
import { storage } from 'core/browser';

const baseURL = 'https://parking.goodline.info/api';

export const api = axios.create({
  baseURL
});

const addToken = (config) => {
  const token = storage.getToken();

  if (token !== null) {
    /* eslint-disable no-param-reassign */
    config.headers.Authorization = `Bearer ${token}`;
  }

  return config;
};

const rejectError = error => Promise.reject(error);


api.interceptors.request.use(addToken, rejectError);
