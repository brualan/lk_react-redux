import React from 'react';
import ReactDOM from 'react-dom';
import { history } from 'core/browser';
import './index.sass';

import { Root } from './Root';

const MOUNT_NODE = document.getElementById('app');

const render = (Component) => {
  ReactDOM.render(
    <Component history={history} />,
    MOUNT_NODE
  );
};


render(Root);

if (module.hot) {
  module.hot.accept('./Root', () => {
    /* eslint-disable global-require */
    const { Root: UpdatedRoot } = require('./Root');
    render(UpdatedRoot);
  });
}
