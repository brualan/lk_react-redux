import { pipe, prop } from 'ramda';

export const getAuth = prop('authentication');
export const getUser = pipe(getAuth, prop('user'));
export const getToken = pipe(getAuth, prop('token'));
export const isPending = pipe(getAuth, prop('isPending'));
export const isRegistred = pipe(getAuth, prop('isRegistred'));
export const isCodeSending = pipe(getAuth, prop('isCodeSending'));
export const getUserPhone = pipe(getUser, prop('phone'));

export const isAuth = pipe(
  getAuth,
  state => state.token !== null
);

export const isFirstLogin = (state) => {
  const user = getUser(state);

  if (user === null) {
    return true;
  }

  return user.telegram_id === null;
};

export const shouldShowTelegramGuide = (state) => {
  const { telegramGuideHidden, user } = getAuth(state);

  if (telegramGuideHidden) {
    return false;
  }

  return user.telegram_id == null;
};
