import * as types from '../constants';

export const login = credentials => ({
  type: types.LOGIN.REQUEST,
  payload: {
    credentials
  }
});

export const successLogin = token => ({
  type: types.LOGIN.SUCCESS,
  payload: {
    token
  }
});

export const failureLogin = error => ({
  type: types.LOGIN.FAILURE,
  error
});

export const firstLogin = phone => ({
  type: types.FIRST_LOGIN.REQUEST,
  payload: {
    phone
  }
});

export const successFirstLogin = token => ({
  type: types.FIRST_LOGIN.SUCCESS,
  payload: {
    token
  }
});

export const failureFirstLogin = error => ({
  type: types.FIRST_LOGIN.FAILURE,
  error
});

export const logout = () => ({
  type: types.LOGOUT
});

export const register = phone => ({
  type: types.REGISTER.REQUEST,
  payload: {
    phone
  }
});

export const successRegister = user => ({
  type: types.REGISTER.SUCCESS,
  payload: {
    user
  }
});

export const failureRegister = error => ({
  type: types.REGISTER.FAILURE,
  error
});

export const userAlreadyExists = user => ({
  type: types.USER_ALREADY_EXISTS,
  payload: {
    user
  }
});

export const sendCode = phone => ({
  type: types.SEND_CODE.REQUEST,
  payload: {
    phone
  }
});

export const successSendCode = () => ({
  type: types.SEND_CODE.SUCCESS
});

export const failureSendCode = () => ({
  type: types.SEND_CODE.FAILURE
});

export const hideTelegramGuide = () => ({
  type: types.HIDE_TELEGRAM_GUIDE
});
