import { storage } from 'core/browser';
import { LOGIN, LOGOUT, SEND_CODE, USER_ALREADY_EXISTS, FIRST_LOGIN, REGISTER, HIDE_TELEGRAM_GUIDE } from './constants';

const initialState = {
  user: storage.getUser(),
  token: storage.getToken(),
  telegramGuideHidden: !!storage.telegramGuideHidden(),
  isCodeSending: false,
  // if user already registred, but not authorized
  isRegistred: storage.getToken() !== null,
  isPending: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
  case LOGIN.REQUEST:
    return {
      ...state,
      isPending: true
    };

  case LOGIN.SUCCESS:
    return {
      ...state,
      token: action.payload.token,
      isPending: false
    };

  case LOGIN.FAILURE:
    return initialState;

  case REGISTER.REQUEST:
  case FIRST_LOGIN.REQUEST:
    return { ...state, isPending: true };

  case REGISTER.SUCCESS:
    return {
      ...state,
      user: {
        telegram_id: null,
        ...state.user,
        ...action.payload.user
      },
      isRegistred: true,
      isPending: false
    };

  case FIRST_LOGIN.SUCCESS:
    return {
      ...state,
      token: action.payload.token,
      isPending: false
    };

  case REGISTER.FAILURE:
  case FIRST_LOGIN.FAILURE:
    return { ...state, isPending: false };

  case LOGOUT:
    return {
      ...state,
      token: null,
      user: null,
      telegramGuideHidden: false,
      isRegistred: false
    };

  case USER_ALREADY_EXISTS:
    return {
      ...state,
      user: action.payload.user,
      isRegistred: true
    };
  case SEND_CODE.REQUEST:
    return {
      ...state,
      isCodeSending: true,
      isPending: true
    };
  case SEND_CODE.SUCCESS:
  case SEND_CODE.FAILURE:
    return {
      ...state,
      isCodeSending: false,
      isPending: false
    };

  case HIDE_TELEGRAM_GUIDE:
    return {
      ...state,
      telegramGuideHidden: true
    };
  default:
    return state;
  }
}
