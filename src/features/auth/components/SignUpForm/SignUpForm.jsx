import React from 'react';
import { Form } from 'core/components/Form';
import { Button } from 'core/components/Button';
import { Gutter } from 'core/components/Gutter';
import { Spinner } from 'core/components/Spinner';
import { PhoneInput } from 'core/components/PhoneInput';
// import { CodeField } from './CodeField';
import './SignUpForm.sass';

export const SignUpForm = props => (
  <Form className="sign-up" onSubmit={props.onSubmit} disabled={props.isPending}>
    <Form.Title level="h4" className="sign-up__title">
      Личный кабинет
    </Form.Title>

    <Form.Field label="Введите номер телефона">
      <PhoneInput
        className="sign-up__phone"
        value={props.phone}
        onChange={props.onChange}
        readOnly={props.isRegistred}
        type="phone"
        name="phone"
        autoFocus
        required
      />
    </Form.Field>

    <Gutter size={0.5} />

    <Form.Note>
      Номер нужен для входа в сервис парковки и общения с Telegram-ботом
    </Form.Note>

    <Gutter size={1} />

    {/* props.isRegistred && !props.isFirstLogin && (
      <CodeField value={props.code} onChange={props.onChange}>
        <Button
          disabled={props.isCodeSending || props.phone.replace(/\D/g, '').length !== 11}
          onClick={props.onCodeRequest}
          skin="secondary"
          className="sign-up__send-button"
          type="button"
        >
          Отправить код
        </Button>
      </CodeField>
    ) */}

    {/* props.isRegistred && props.isFirstLogin && (
      <CodeField value={props.code} onChange={props.onChange}>
        <Button
          as="a"
          skin="secondary"
          className="sign-up__send-button"
          href="http://t.me/autoparkingbot"
          rel="noreferrer noopener"
          target="_blank"
        >
            Добавить телеграм бота
        </Button>
      </CodeField>
    ) */}

    <Gutter size={1} />

    {props.isPending
      ? <Spinner absolute={false} />
      : (
        <Button className="sign-up__submit" type="submit">
          Войти
        </Button>
      )
    }
  </Form>
);

