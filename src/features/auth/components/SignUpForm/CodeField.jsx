import React from 'react';
import { TextInput } from 'core/components/TextInput';

export const CodeField = props => (
  <div className="sign-up__code-field">
    <TextInput
      id="password"
      type="password"
      name="code"
      placeholder="Код от бота"
      className="sign-up__code-input"
      onChange={props.onChange}
      value={props.value}
      autoFocus
      required
    />
    {props.children}
  </div>
);
