import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Container } from 'core/components/Container';
import { shouldShowTelegramGuide } from 'features/auth/selectors';
import { hideTelegramGuide } from 'features/auth/actions';
import one from './img/if-number-one.png';
import one2x from './img/if-number-one@2x.png';
import two from './img/if-number-two.png';
import two2x from './img/if-number-two@2x.png';
import three from './img/if-number-three.png';
import three2x from './img/if-number-three@2x.png';
import './Jumbotron.sass';

/* eslint-disable jsx-a11y/anchor-has-content */
const Link = props => (
  <a
    className="jumbotron__link"
    rel="noreferrer noopener"
    target="_blank"
    {...props}
  />
);

export class Jumbotron extends React.Component {
  shouldComponentUpdate(nextProps) {
    return this.props.hidden !== nextProps.hidden;
  }

  render() {
    if (this.props.hidden) {
      return null;
    }

    return (
      <div className="jumbotron">
        <Container className="jumbotron__container">
          <div className="jumbotron__title">
            {this.props.renderTitle()}
          </div>
          <div className="jumbotron__steps">
            <div className="jumbotron__step">
              <div className="jumbotron__step-image">
                <img src={one} srcSet={`${one2x} 2x`} alt="1" />
              </div>
              <div className="jumbotron__step-text">
                {this.props.renderFirstStep()}
              </div>
            </div>
            <div className="jumbotron__step">
              <div className="jumbotron__step-image">
                <img src={two} srcSet={`${two2x} 2x`} alt="2" />
              </div>
              <div className="jumbotron__step-text">
                {this.props.renderSecondStep()}
              </div>
            </div>
            <div className="jumbotron__step">
              <div className="jumbotron__step-image">
                <img src={three} srcSet={`${three2x} 2x`} alt="3" />
              </div>
              <div className="jumbotron__step-text">
                {this.props.renderThirdStep()}
              </div>
            </div>
          </div>
          {this.props.hides && (
            <button title="Скрыть" className="jumbotron__hide" onClick={this.props.onHide}>
              Скрыть, я все выполнил
            </button>
          )}
        </Container>
      </div>
    );
  }
}

const renderTelegramLinks = () => (
  <Fragment>
    Скачайте Telegram на телефон <br /> (
    <Link href="https://telegram.org/dl/ios">AppStore</Link>
    {' '} и {' '}
    <Link href="https://telegram.org/dl/android">Google Play</Link>
    )
    <nobr>
      {' '} или <Link href="https://desktop.telegram.org/">компьютер</Link>
    </nobr>
  </Fragment>
);

export const AuthJumbotron = () => (
  <Jumbotron
    renderTitle={() => (
      <Fragment>
        <span>Найдите</span> своё парковочное место. Просто
      </Fragment>
    )}
    renderFirstStep={renderTelegramLinks}
    renderSecondStep={() => (
      'Зарегистрируйтесь по номеру телефона'
    )}
    renderThirdStep={() => (
      'Получите ссылку на бота в Telegram с видео-камерами парковки'
    )}
  />
);

const connectTelegramJumbotron = connect(
  state => ({
    hidden: !shouldShowTelegramGuide(state)
  }), {
    onHide: hideTelegramGuide
  }
);

export const TelegramJumbotron = connectTelegramJumbotron(props => (
  <Jumbotron
    hides
    hidden={props.hidden}
    onHide={props.onHide}
    renderTitle={() => (
      <Fragment>
        Чтобы увидеть свою камеру, авторизуйтесь в <nobr><span>Telegram-боте</span></nobr>.
      </Fragment>
    )}
    renderFirstStep={renderTelegramLinks}
    renderSecondStep={() => (
      <Link href="http://t.me/autoparkingbot" className="jumbotron__link jumbotron__link_bot">
        Добавьте Telegram-бота
      </Link>
    )}
    renderThirdStep={() => (
      'Нажмите начать пользоваться'
    )}
  />
));
