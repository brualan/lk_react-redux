import React from 'react';
import { connect } from 'react-redux';
import { login } from '../actions';

const withStore = connect(null, {
  onLogin: login
});

class LoginFormContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      phone: '',
      code: ''
    };
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const { phone, code } = this.state;

    if (phone && code) {
      this.props.onLogin({ phone, code });
    }
  };

  render() {
    const { phone, code } = this.state;
    return (
      <div className="modal fade" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <button type="button" id="hidePopUpBtn" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h2>Вход</h2>
              <form name="form" onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <label htmlFor="phone">Номер</label>
                  <input type="text" className="form-control" name="phone" value={phone} onChange={this.handleChange} />
                </div>
                <div className="form-group">
                  <label htmlFor="password">Пароль</label>
                  <input type="password" className="form-control" name="code" value={code} onChange={this.handleChange} />
                </div>
                <div className="form-group">
                  <button className="btn btn-primary">Войти</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStore(LoginFormContainer);
