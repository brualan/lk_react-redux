import React from 'react';
import qs from 'query-string';
import { compose } from 'ramda';
import { Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { login, register, sendCode } from '../actions';
import * as selectors from '../selectors';
import SignUpForm from './SignUpForm';

const onlyDigits = string => string.replace(/\D/g, '');

class SignUpFormContainer extends React.Component {
  constructor(props) {
    super(props);

    const params = qs.parse(props.location.search);

    this.state = {
      phone: params.phone || '',
      code: ''
    };

    if (params.phone !== undefined) {
      props.onRegiser(params.phone);
    }
  }

  sendCode = (e) => {
    e.preventDefault();
    this.props.onSendCode(onlyDigits(this.state.phone));
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const { phone: maskedPhone, code } = this.state;
    const phone = onlyDigits(maskedPhone);

    const shouldLogin = phone.trim() !== '' && code.trim() !== '';

    if (shouldLogin) {
      this.props.onLogin({ phone, code });
      return;
    }

    this.props.onRegiser(phone);
  }
  render() {
    if (this.props.isAuth) {
      return <Redirect to="/" replace />;
    }

    return (
      <SignUpForm
        code={this.state.code}
        phone={this.state.phone}
        isRegistred={this.props.isRegistred}
        isFirstLogin={this.props.isFirstLogin}
        isCodeSending={this.props.isCodeSending}
        isPending={this.props.isPending}
        onChange={this.handleChange}
        onSubmit={this.handleSubmit}
        onCodeRequest={this.sendCode}
      />
    );
  }
}

const withStore = connect(state => ({
  isAuth: selectors.isAuth(state),
  isPending: selectors.isPending(state),
  isRegistred: selectors.isRegistred(state),
  isFirstLogin: selectors.isFirstLogin(state),
  isCodeSending: selectors.isCodeSending(state)
}), {
  onSendCode: sendCode,
  onLogin: login,
  onRegiser: register
});

const enhance = compose(
  withRouter,
  withStore
);

export default enhance(SignUpFormContainer);
