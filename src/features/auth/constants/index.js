import { createAsyncType, createType, withNamespace } from 'store/utils';

export const NAMESPACE = 'AUTH';

const type = withNamespace(NAMESPACE, createType);
const asyncType = withNamespace(NAMESPACE, createAsyncType);

export const LOGIN = asyncType('LOGIN');
export const FIRST_LOGIN = asyncType('FIRST_LOGIN');
export const REGISTER = asyncType('REGISTER');
export const LOGOUT = type('LOGOUT');
export const SEND_CODE = asyncType('SEND_CODE');
export const USER_ALREADY_EXISTS = type('USER_ALREADY_EXISTS');
export const HIDE_TELEGRAM_GUIDE = type('HIDE_TELEGRAM_GUIDE');
