import { ofType } from 'redux-observable';
import { catchError, map, switchMap } from 'rxjs/operators';
import { failureSendCode, successSendCode } from '../actions';
import { SEND_CODE } from '../constants';

const toRequest = ({ payload }) => ({
  url: '/forgot-password',
  method: 'POST',
  data: {
    phone: payload.phone
  }
});

export function sendCodeEpic(action$, store, { fromApi }) {
  return action$.pipe(
    ofType(SEND_CODE.REQUEST),
    map(toRequest),
    switchMap(request =>
      fromApi(request).pipe(
        map(successSendCode),
        catchError(reason => [failureSendCode(reason.response)]),
      )),
  );
}
