import { combineEpics } from 'redux-observable';
import { loginEpic } from './login';
import { logoutEpic } from './logout';
import { registerEpic } from './register';
import { sendCodeEpic } from './sendCode';
import { telegramGuideEpic } from './telegramGuide';

export const auth = combineEpics(
  loginEpic,
  logoutEpic,
  registerEpic,
  sendCodeEpic,
  telegramGuideEpic
);
