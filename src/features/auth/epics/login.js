import { ofType } from 'redux-observable';
import { merge as mergeAll } from 'rxjs/observable/merge';
import { map, switchMap, catchError, tap } from 'rxjs/operators';

import * as actions from '../actions';
import { LOGIN, FIRST_LOGIN } from '../constants';

const onlyDigits = string => string.replace(/\D/g, '');

const toRequest = ({ payload: { credentials } }) => ({
  url: '/login',
  method: 'POST',
  data: {
    phone: onlyDigits(credentials.phone),
    password: credentials.code
  }
});

const nextTick = setTimeout;

export function loginEpic(action$, store, { fromApi, history, storage }) {
  const doRequest = request => fromApi(request).pipe(
    map(response => response.data.message.token),
    tap((token) => {
      storage.saveToken(token);
      // change location after store update
      nextTick(() => { history.push('/'); });
    })
  );

  const login$ = action$.pipe(
    ofType(LOGIN.REQUEST),
    map(toRequest),
    switchMap(doRequest),
    map(actions.successLogin),
    catchError(reason => [
      actions.failureLogin(reason)
    ])
  );

  const firstLogin$ = action$.pipe(
    ofType(FIRST_LOGIN.REQUEST),
    map(({ payload: { phone } }) => ({
      url: '/first-login',
      method: 'POST',
      data: {
        phone: phone.replace(/\D/g, '')
      }
    })),
    switchMap(doRequest),
    map(actions.successFirstLogin),
    catchError(reason => [
      actions.failureFirstLogin(reason)
    ])
  );

  return mergeAll(login$, firstLogin$);
}
