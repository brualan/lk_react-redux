import { ofType } from 'redux-observable';
import { tap, ignoreElements } from 'rxjs/operators';
import { HIDE_TELEGRAM_GUIDE } from '../constants';

export function telegramGuideEpic(action$, store, { storage }) {
  return action$.pipe(
    ofType(HIDE_TELEGRAM_GUIDE),
    tap(() => {
      storage.telegramGuideHidden(true);
    }),
    ignoreElements()
  );
}
