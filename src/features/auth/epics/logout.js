import { ofType } from 'redux-observable';
import { mapTo, tap } from 'rxjs/operators';
import { LOGOUT } from '../constants';

export function logoutEpic(action$, store, { storage }) {
  return action$.pipe(
    ofType(LOGOUT),
    tap(() => {
      storage.clean();
    }),
    mapTo({ type: '@@STORAGE_CLEARED' }),
  );
}
