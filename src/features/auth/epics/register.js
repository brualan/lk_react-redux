import { ofType } from 'redux-observable';
// import { merge as mergeAll } from 'rxjs/observable/merge';
import { map, switchMap, flatMap, catchError, tap } from 'rxjs/operators';
// import { actions as alerts, statuses } from 'features/alerts';

import { failureRegister, userAlreadyExists, firstLogin, successRegister } from '../actions';
import { REGISTER } from '../constants';

const toRequest = ({ payload }) => ({
  url: '/register',
  method: 'POST',
  data: {
    phone: payload.phone.replace(/\D/g, '')
  }
});

export function registerEpic(action$, store, { fromApi, storage }) {
  const register$ = action$.pipe(
    ofType(REGISTER.REQUEST),
    map(toRequest),
    switchMap(request =>
      fromApi(request).pipe(
        map(response => response.data.data),
        tap((user) => {
          storage.saveUser(user);
        }),
        flatMap(user => [
          successRegister(user),
          firstLogin(user.phone)
        ]),
        catchError((reason) => {
          const actions = [failureRegister(reason.response)];

          // User already exists
          if (reason.response.status === 401) {
            const user = reason.response.data.data;
            storage.saveUser(user);
            actions.push(userAlreadyExists(user));
            actions.push(firstLogin(user.phone));
          }

          return actions;
        })
      )
    )
  );

  // const alerts$ = action$.pipe(
  //   ofType(USER_ALREADY_EXISTS),
  //   map(() => alerts.add({
  //     status: statuses.WARNING,
  //     message: 'Вы уже зарегистрированы!',
  //     timeout: 3000
  //   }))
  // );

  return register$;
}
