import * as types from '../constants/action-types';

export const fetchCameras = () => ({
  type: types.FETCH_CAMERAS.REQUEST
});

export const receiveCameras = cameras => ({
  type: types.FETCH_CAMERAS.SUCCESS,
  payload: cameras
});

export const failFetchCameras = error => ({
  type: types.FETCH_CAMERAS.FAILURE,
  error
});

export const addCamera = camera => ({
  type: types.ADD_CAMERA.REQUEST,
  payload: camera
});

export const receiveCamera = camera => ({
  type: types.ADD_CAMERA.SUCCESS,
  payload: camera
});

export const failAddCamera = error => ({
  type: types.ADD_CAMERA.FAILURE,
  error
});

export const fetchImage = id => ({
  type: types.FETCH_IMAGE.REQUEST,
  payload: {
    id
  }
});

export const receiveImage = ({ id, image }) => ({
  type: types.FETCH_IMAGE.SUCCESS,
  meta: { id },
  payload: image
});

export const failFetchImage = error => ({
  type: types.FETCH_IMAGE.FAILURE,
  error
});

export const deleteCamera = id => ({
  type: types.DELETE_CAMERA.REQUEST,
  payload: {
    id
  }
});

export const successDeleteCamera = id => ({
  type: types.DELETE_CAMERA.SUCCESS,
  payload: {
    id
  }
});

export const failDeleteCamera = error => ({
  type: types.DELETE_CAMERA.FAILURE,
  error
});

export const saveDots = ({ camera_id, parking_spaces_count, dots }) => ({
  type: types.SAVE_DOTS.REQUEST,
  payload: {
    camera_id,
    parking_spaces_count,
    dots
  }
});

export const searchManufacturer = ({ value, page = 1 }) => ({
  type: types.SEARCH_MANUFACTURER.REQUEST,
  payload: {
    search: value,
    page
  }
});

export const searchModel = ({ manufacturer_id, value, page = 1 }) => ({
  type: types.SEARCH_MODEL.REQUEST,
  payload: {
    manufacturer_id,
    search: value,
    page
  }
});

export const updateCamera = camera => ({
  type: types.UPDATE_CAMERA.REQUEST,
  payload: camera
});

export const addViewer = ({ camera_id, phone }) => ({
  type: types.ADD_VIEWER.REQUEST,
  payload: {
    camera_id,
    phone
  }
});

export const removeViewer = ({ user_id, camera_id }) => ({
  type: types.REMOVE_VIEWER.REQUEST,
  payload: {
    user_id,
    camera_id
  }
});
