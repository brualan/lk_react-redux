import { createAsyncType, withNamespace } from 'store/utils';

export const NAMESPACE = 'CAMERAS';

const asyncType = withNamespace(NAMESPACE, createAsyncType);

export const SEARCH_MANUFACTURER = asyncType('SEARCH_MANUFACTURER');
export const SEARCH_MODEL = asyncType('SEARCH_MODEL');

export const FETCH_CAMERAS = asyncType('FETCH_CAMERAS');
export const ADD_CAMERA = asyncType('ADD_CAMERA');
export const UPDATE_CAMERA = asyncType('UPDATE_CAMERA');
export const DELETE_CAMERA = asyncType('DELETE_CAMERA');

export const FETCH_IMAGE = asyncType('FETCH_IMAGE');
export const SAVE_DOTS = asyncType('SAVE_DOTS');

export const ADD_VIEWER = asyncType('ADD_VIEWER');
export const REMOVE_VIEWER = asyncType('REMOVE_VIEWER');
