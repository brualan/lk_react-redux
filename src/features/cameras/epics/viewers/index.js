import { combineEpics } from 'redux-observable';
import { addViewerEpic } from './addViewer';
import { removeViewerEpic } from './removeViewer';

export const viewersEpic = combineEpics(
  addViewerEpic,
  removeViewerEpic
);
