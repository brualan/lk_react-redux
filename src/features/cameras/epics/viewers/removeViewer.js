import { ofType } from 'redux-observable';
import { map, flatMap, catchError, tap } from 'rxjs/operators';
import * as types from 'features/cameras/constants/action-types';

const toRequest = ({ payload }) => ({
  url: '/user/camera/viewer/delete',
  method: 'POST',
  data: payload
});

export function removeViewerEpic(action$, store, { fromApi, history }) {
  return action$.pipe(
    ofType(types.REMOVE_VIEWER.REQUEST),
    map(toRequest),
    flatMap(request =>
      fromApi(request).pipe(
        map(() => ({
          type: types.REMOVE_VIEWER.SUCCESS,
          payload: request.data
        })),
        tap(() => {
          history.push('/');
        }),
        catchError(error => [{
          type: types.REMOVE_VIEWER.FAILURE,
          error
        }])
      )
    )
  );
}
