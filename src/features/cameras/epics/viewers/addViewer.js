import { ofType } from 'redux-observable';
import { map, flatMap, catchError, tap } from 'rxjs/operators';
import * as types from 'features/cameras/constants/action-types';

const toRequest = ({ payload }) => ({
  url: '/user/camera/viewer',
  method: 'POST',
  data: {
    camera_id: payload.camera_id,
    phone: payload.phone.replace(/\D/g, '')
  }
});

export function addViewerEpic(action$, store, { fromApi, history }) {
  return action$.pipe(
    ofType(types.ADD_VIEWER.REQUEST),
    map(toRequest),
    flatMap(request =>
      fromApi(request).pipe(
        map(response => ({
          type: types.ADD_VIEWER.SUCCESS,
          payload: response.data.data || []
        })),
        tap(() => {
          history.push('/');
        }),
        catchError(error => [{
          type: types.ADD_VIEWER.FAILURE,
          error
        }])
      )
    )
  );
}
