import { ofType } from 'redux-observable';
import { map, flatMap, catchError, tap } from 'rxjs/operators';
import { actions as alerts } from 'features/alerts';
import * as types from '../constants/action-types';

const toRequest = ({ payload }) => ({
  url: '/user/camera/dots',
  method: 'POST',
  data: payload
});

export function saveDotsEpic(action$, store, { fromApi, history }) {
  return action$.pipe(
    ofType(types.SAVE_DOTS.REQUEST),
    map(toRequest),
    flatMap(request =>
      fromApi(request).pipe(
        map(() => ({
          type: types.SAVE_DOTS.SUCCESS,
          payload: request.data
        })),
        tap(() => {
          history.push('/');
        }),
        catchError(error => [
          {
            type: types.SAVE_DOTS.FAILURE,
            error
          },
          alerts.add({
            status: 'danger',
            message: 'Ошибка при сохранении камеры!',
            timeout: 3500
          })
        ])
      )
    )
  );
}
