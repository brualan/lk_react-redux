import { ofType } from 'redux-observable';
import { zip } from 'rxjs/observable/zip';
import { range } from 'rxjs/observable/range';
import { timer } from 'rxjs/observable/timer';
import { of } from 'rxjs/observable/of';
import { merge } from 'rxjs/observable/merge';
import { map, pluck, retryWhen, flatMap, catchError, tap, debounceTime, takeUntil } from 'rxjs/operators';
import { actions as alerts } from 'features/alerts';
import * as types from '../constants/action-types';
import { receiveImage } from '../actions';

const toRequest = ({ payload }) => ({
  url: '/user/camera/image',
  method: 'GET',
  params: {
    camera_id: payload.id
  }
});

export function fetchImageEpic(action$, store, { history, fromApi }) {
  const shouldRepeat = errors$ => zip(errors$, range(1, 8)).pipe(
    flatMap(([error, i]) => {
      if (i <= 7) {
        return timer(1000);
      }

      throw error;
    })
  );

  const fetchUrl = action => (
    fromApi(toRequest(action)).pipe(
      pluck('data', 'data', 'image'),
      map((url) => {
        if (!url) {
          throw Error('EMPTY_IMAGE_URL');
        }

        return url;
      }),
      // if image url is empty (backend has not yet created the image)
      // repeat request 8 times with interval by 1 second
      retryWhen(shouldRepeat),
      map(image => receiveImage({ id: action.payload.id, image })),
      catchError(error => of({
        type: types.FETCH_IMAGE.FAILURE,
        payload: error,
        error: true
      }))
    )
  );

  const image$ = action$.pipe(
    ofType(types.FETCH_IMAGE.REQUEST),
    flatMap(fetchUrl),
    tap((action) => {
      if (action.type === types.FETCH_IMAGE.FAILURE) {
        history.push('/');
      }
    })
  );

  const alerts$ = merge(
    action$.pipe(
      ofType(types.FETCH_IMAGE.REQUEST),
      debounceTime(1000),
      takeUntil(action$.ofType(types.FETCH_IMAGE.SUCCESS)),
      map(() => alerts.add({
        status: 'info',
        message: 'Пытаемся получить изображение с камеры.',
        timeout: 2000
      }))
    ),
    action$.pipe(
      ofType(types.FETCH_IMAGE.FAILURE),
      map(() => alerts.add({
        status: 'danger',
        message: 'Не удалось получить изображение камеры.',
        timeout: 4000
      }))
    )
  );

  return merge(image$, alerts$);
}
