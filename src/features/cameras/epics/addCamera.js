import { ofType } from 'redux-observable';
import { merge } from 'rxjs/observable/merge';
import { map, pluck, flatMap, catchError, tap, ignoreElements } from 'rxjs/operators';
import { actions as alerts } from 'features/alerts';
import { receiveCamera, failAddCamera } from '../actions';
import * as types from '../constants/action-types';

const toRequest = ({ payload }) => ({
  url: '/user/camera',
  method: 'POST',
  data: payload
});

const handleError = (reason) => {
  let message;

  switch (reason.response.status) {
  case 401:
    message = 'Вы не можете добавить еще одну камеру!';
    break;
  case 406:
    message = 'Камера с таким адресом уже существует в системе. Обратитесь к владельцу камеры';
    break;
  default:
    message = 'Не удалось добавить камеру!';
  }

  return [
    failAddCamera(reason),
    alerts.add({
      status: 'danger',
      message,
      timeout: 8000
    })
  ];
};

export function addCameraEpic(action$, store, { fromApi, history }) {
  const fromRequest = request => (
    fromApi(request).pipe(
      pluck('data', 'data'),
      map(receiveCamera),
      catchError(handleError)
    )
  );

  const addCamera$ = action$.pipe(
    ofType(types.ADD_CAMERA.REQUEST),
    map(toRequest),
    flatMap(fromRequest)
  );

  const success$ = action$.pipe(
    ofType(types.ADD_CAMERA.SUCCESS),
    tap((action) => {
      const { id, camera_model_id } = action.payload;

      if (!camera_model_id) {
        history.push(`/cameras/${id}/manufacturer`);
      } else {
        history.push(`/cameras/${id}/image`);
      }
    }),
    ignoreElements()
  );

  return merge(addCamera$, success$);
}
