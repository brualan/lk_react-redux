import { ofType } from 'redux-observable';
import { map, pluck, flatMap, catchError } from 'rxjs/operators';
import { receiveCameras, failFetchCameras } from '../actions';
import * as types from '../constants/action-types';

const toRequest = () => ({
  url: '/user/cameras/viewers',
  method: 'GET'
});

export function fetchCamerasEpic(action$, store, { fromApi }) {
  const fromRequest = request => (
    fromApi(request).pipe(
      pluck('data', 'data'),
      map(receiveCameras),
      catchError(reason => [
        failFetchCameras(reason)
      ])
    )
  );

  return action$.pipe(
    ofType(types.FETCH_CAMERAS.REQUEST),
    map(toRequest),
    flatMap(fromRequest)
  );
}
