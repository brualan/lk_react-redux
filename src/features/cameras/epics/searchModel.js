import { ofType } from 'redux-observable';
import { map, pluck, switchMap, debounceTime, catchError } from 'rxjs/operators';
import * as types from '../constants/action-types';

const toRequest = ({ payload }) => ({
  url: '/user/cameras/models',
  method: 'GET',
  params: {
    camera_manufacturer_id: payload.manufacturer_id,
    search: payload.search,
    page: payload.page
  }
});

export function searchModelEpic(action$, store, { fromApi }) {
  return action$.pipe(
    ofType(types.SEARCH_MODEL.REQUEST),
    map(toRequest),
    debounceTime(500),
    switchMap(request =>
      fromApi(request).pipe(
        pluck('data', 'data'),
        map(payload => ({
          type: types.SEARCH_MODEL.SUCCESS,
          payload,
          meta: {
            manufacturer_id: request.params.camera_manufacturer_id
          }
        })),
        catchError(error => [{
          type: types.SEARCH_MODEL.FAILURE,
          error
        }])
      )
    )
  );
}
