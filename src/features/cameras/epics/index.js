import { combineEpics } from 'redux-observable';
import { fetchCamerasEpic } from './fetchCameras';
import { addCameraEpic } from './addCamera';
import { fetchImageEpic } from './fetchImage';
import { deleteCameraEpic } from './deleteCamera';
import { saveDotsEpic } from './saveDots';
import { searchManufacturerEpic } from './searchManufacturer';
import { searchModelEpic } from './searchModel';
import { updateCameraEpic } from './updateCamera';
import { viewersEpic } from './viewers';

export const epic = combineEpics(
  fetchCamerasEpic,
  fetchImageEpic,
  addCameraEpic,
  updateCameraEpic,
  deleteCameraEpic,
  saveDotsEpic,
  searchManufacturerEpic,
  searchModelEpic,
  viewersEpic
);
