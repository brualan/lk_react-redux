import { ofType } from 'redux-observable';
import { map, flatMap, catchError } from 'rxjs/operators';
import { successDeleteCamera, failDeleteCamera } from '../actions';
import * as types from '../constants/action-types';

const toRequest = ({ payload }) => ({
  url: '/user/camera/delete',
  method: 'POST',
  data: {
    camera_id: payload.id
  }
});

export function deleteCameraEpic(action$, store, { fromApi }) {
  return action$.pipe(
    ofType(types.DELETE_CAMERA.REQUEST),
    map(toRequest),
    flatMap(request =>
      fromApi(request).pipe(
        map(() => request.data.camera_id),
        map(successDeleteCamera),
        catchError(reason => [
          failDeleteCamera(reason)
        ])
      )
    )
  );
}
