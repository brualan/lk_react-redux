import { ofType } from 'redux-observable';
import { map, switchMap, catchError, tap } from 'rxjs/operators';
import * as types from '../constants/action-types';

const toRequest = ({ payload }) => ({
  url: '/user/camera/update',
  method: 'POST',
  data: payload
});

export function updateCameraEpic(action$, store, { fromApi, history }) {
  return action$.pipe(
    ofType(types.UPDATE_CAMERA.REQUEST),
    map(toRequest),
    switchMap(request =>
      fromApi(request).pipe(
        map(() => ({
          type: types.UPDATE_CAMERA.SUCCESS,
          payload: request.data
        })),
        tap(() => {
          history.push(`/cameras/${request.data.camera_id}/image`);
        }),
        catchError(error => [{
          type: types.SEARCH_MODEL.FAILURE,
          error,
          meta: request.data
        }])
      )
    )
  );
}
