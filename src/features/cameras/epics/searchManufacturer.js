import { ofType } from 'redux-observable';
import { map, pluck, switchMap, debounceTime, catchError } from 'rxjs/operators';
import * as types from '../constants/action-types';

const toRequest = ({ payload }) => ({
  url: '/user/cameras/manufacturers',
  method: 'GET',
  params: {
    search: payload.search,
    limit: 50
  }
});

export function searchManufacturerEpic(action$, store, { fromApi }) {
  return action$.pipe(
    ofType(types.SEARCH_MANUFACTURER.REQUEST),
    map(toRequest),
    debounceTime(500),
    switchMap(request =>
      fromApi(request).pipe(
        pluck('data', 'data'),
        map(payload => ({
          type: types.SEARCH_MANUFACTURER.SUCCESS,
          payload
        })),
        catchError(error => [{
          type: types.SEARCH_MANUFACTURER.FAILURE,
          error
        }])
      )
    )
  );
}
