import { cameras } from './cameras';
import { manufacturers } from './manufacturers';
import { models } from './models';
import { viewers } from './viewers';
import { pendings } from './pendings';

export const entities = {
  cameras,
  viewers
};

export const ui = {
  pendings,
  manufacturers,
  models
};
