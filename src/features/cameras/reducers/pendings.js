import * as types from '../constants/action-types';

const INITIAL_STATE = {
  camera: false,
  image: false,
  cameras: false,
  manufacturers: false,
  models: false,
  dots: false,
  viewer: false,
  viewers: false
};

export function pendings(state = INITIAL_STATE, action) {
  switch (action.type) {
  case types.ADD_CAMERA.REQUEST:
  case types.UPDATE_CAMERA.REQUEST:
    return {
      ...state,
      camera: true
    };

  case types.FETCH_IMAGE.REQUEST:
    return { ...state, image: true };

  case types.ADD_VIEWER.REQUEST:
    return { ...state, viewer: true };

  case types.ADD_VIEWER.SUCCESS:
  case types.ADD_VIEWER.FAILURE:
    return { ...state, viewer: false };

  case types.ADD_CAMERA.SUCCESS:
  case types.ADD_CAMERA.FAILURE:
  case types.UPDATE_CAMERA.SUCCESS:
  case types.UPDATE_CAMERA.FAILURE:
    return {
      ...state,
      camera: false
    };

  case types.FETCH_IMAGE.SUCCESS:
  case types.FETCH_IMAGE.FAILURE:
    return {
      ...state,
      image: false
    };

  case types.FETCH_CAMERAS.REQUEST:
  case types.DELETE_CAMERA.REQUEST:
    return {
      ...state,
      cameras: true,
      viewers: true
    };

  case types.DELETE_CAMERA.SUCCESS:
  case types.DELETE_CAMERA.FAILURE:
  case types.FETCH_CAMERAS.SUCCESS:
  case types.FETCH_CAMERAS.FAILURE:
    return {
      ...state,
      cameras: false,
      viewers: false
    };

  case types.REMOVE_VIEWER.REQUEST:
    return {
      ...state,
      viewers: true
    };

  case types.REMOVE_VIEWER.SUCCESS:
  case types.REMOVE_VIEWER.FAILURE:
    return {
      ...state,
      viewers: false
    };

  case types.SAVE_DOTS.REQUEST:
    return {
      ...state,
      camera: true,
      dots: true
    };

  case types.SAVE_DOTS.SUCCESS:
  case types.SAVE_DOTS.FAILURE:
    return {
      ...state,
      camera: false,
      dots: false
    };

  default:
    return state;
  }
}
