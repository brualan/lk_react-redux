import * as types from '../constants/action-types';

export function manufacturers(state = [], action) {
  switch (action.type) {
  case types.SEARCH_MANUFACTURER.SUCCESS:
    return action.payload;
  default:
    return state;
  }
}
