import { chain, indexBy, prop, pipe, omit } from 'ramda';
import * as types from '../constants/action-types';

const indexByID = indexBy(prop('id'));
const indexViewersByID = pipe(
  chain(prop('viewers')),
  indexByID
);

export function viewers(state = {}, action) {
  switch (action.type) {
  case types.FETCH_CAMERAS.SUCCESS:
    return {
      ...indexViewersByID(action.payload),
      ...state
    };
  case types.ADD_VIEWER.SUCCESS:
    return {
      ...state,
      ...indexByID(action.payload)
    };

  case types.REMOVE_VIEWER.SUCCESS:
    return omit([action.payload.user_id], state);

  default:
    return state;
  }
}
