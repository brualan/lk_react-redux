import * as types from '../constants/action-types';

export function models(state = [], action) {
  switch (action.type) {
  case types.SEARCH_MODEL.SUCCESS:
    return action.payload;
  default:
    return state;
  }
}
