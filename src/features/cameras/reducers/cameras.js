import { pipe, map, indexBy, prop, omit, over, lensProp } from 'ramda';
import * as types from '../constants/action-types';

const viewersLens = lensProp('viewers');
const getIDs = map(prop('id'));
const normalizeViewers = over(viewersLens, getIDs);

const indexByID = indexBy(prop('id'));
const normalize = pipe(
  map(normalizeViewers),
  indexByID
);

export function cameras(state = {}, action) {
  switch (action.type) {
  case types.ADD_CAMERA.SUCCESS: {
    return {
      ...state,
      [action.payload.id]: action.payload
    };
  }

  case types.FETCH_CAMERAS.SUCCESS:
    return {
      ...state,
      ...normalize(action.payload)
    };

  case types.FETCH_IMAGE.SUCCESS:
    return {
      ...state,
      [action.meta.id]: {
        ...(state[action.meta.id] || {}),
        image_url: action.payload
      }
    };

  case types.DELETE_CAMERA.SUCCESS:
    return omit([action.payload.id], state);

  default:
    return state;
  }
}
