import { path, values, prop, has, pipe } from 'ramda';
import { createSelector } from 'reselect';

const getEntities = prop('entities');
const getUi = path(['ui', 'cameras']);

const getCamerasMap = pipe(getEntities, prop('cameras'));
const getViewersMap = pipe(getEntities, prop('viewers'));

export const getCameras = createSelector(
  [getCamerasMap], values
);

export const getCameraById = createSelector(
  [(_, id) => id, getCamerasMap], prop
);

export const hasCamera = createSelector(
  [id => id, getCamerasMap], has
);

export const getViewers = createSelector(
  [getViewersMap], values
);

export const getManufacturers = pipe(getUi, prop('manufacturers'));
export const getModels = pipe(getUi, prop('models'));

export const getPendings = pipe(getUi, prop('pendings'));
export const getPendingFor = createSelector(
  [(_, entity) => entity, getPendings], prop
);
