import React from 'react';
import { connect } from 'react-redux';
import { CameraForm } from './CameraForm';
import { addCamera } from '../actions';
import { getPendingFor } from '../selectors';


class CameraFormContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      address: '',
      login: '',
      password: ''
    };
  }

  setValue = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  submit = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state);
  }

  render() {
    return (
      <CameraForm
        values={this.state}
        isPending={this.props.isPending}
        onChange={this.setValue}
        onSubmit={this.submit}
      />
    );
  }
}

const withStore = connect(
  state => ({
    isPending: getPendingFor(state, 'camera')
  }), {
    onSubmit: addCamera
  }
);

export default withStore(CameraFormContainer);
