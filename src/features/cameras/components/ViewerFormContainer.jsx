import React from 'react';
import { connect } from 'react-redux';
import { Form } from 'core/components/Form';
import { Button } from 'core/components/Button';
import { Gutter } from 'core/components/Gutter';
import { Spinner } from 'core/components/Spinner';
import { PhoneInput } from 'core/components/PhoneInput';
import { getPendingFor } from '../selectors';
import { addViewer } from '../actions';
import './ViewerForm.sass';

class ViewerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: ''
    };
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit({
      camera_id: this.props.match.params.id,
      phone: this.state.phone
    });
  }

  render() {
    const { phone } = this.state;
    return (
      <Form className="viewers-form" onSubmit={this.handleSubmit}>
        <Form.Title>Добавление пользователя</Form.Title>
        <Form.Field label="Введите номер пользователя">
          <PhoneInput
            type="text"
            name="phone"
            className="form-control"
            value={phone}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Gutter size={1} />
        {this.props.isPending
          ? <Spinner absolute={false} />
          : (
            <Button type="submit">
              Добавить
            </Button>
          )
        }
      </Form>
    );
  }
}

const withStore = connect(
  state => ({
    isPending: getPendingFor(state, 'viewer')
  }), {
    onSubmit: addViewer
  }
);

export default withStore(ViewerForm);
