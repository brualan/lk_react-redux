import React from 'react';
import { Link } from 'react-router-dom';
import DelteIcon from 'react-icons/lib/md/delete';
import { Spinner } from 'core/components/Spinner';
import { Chip } from 'core/components/Chip';
import { CameraIcon } from './CameraIcon';
import './CamerasList.sass';


const RemoveButton = props => (
  <button className="cameras-list__remove" type="button" {...props}>
    <DelteIcon />
  </button>
);

export const CamerasList = ({ cameras, onDelete, isPending }) => (
  <div className="cameras-list">
    {isPending && (
      <Spinner withOverlay />
    )}

    {!isPending && cameras.map(camera => (
      <div className="cameras-list__item" key={camera.id}>
        <Chip icon={CameraIcon}>
          {camera.name} <RemoveButton onClick={() => onDelete(camera.id)} />
        </Chip>
      </div>
    ))}

    {cameras.length < 1 && (
      <Chip as={Link} className="cameras-list__add" icon={CameraIcon} to="/cameras/new">
        Добавить камеру
      </Chip>
    )}
  </div>
);
