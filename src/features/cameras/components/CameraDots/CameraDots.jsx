import React from 'react';
import MaskedInput from 'react-maskedinput';
import { merge } from 'rxjs/observable/merge';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { takeUntil, filter, flatMap, map as mapObservable } from 'rxjs/operators';
import { assoc, map, update, clamp, identity } from 'ramda';
import pure from 'recompose/pure';
import { Form } from 'core/components/Form';
import { TextInput } from 'core/components/TextInput';
import { Spinner } from 'core/components/Spinner';
import { Button } from 'core/components/Button';
import { Gutter } from 'core/components/Gutter';
import example from './example.png';
import './CameraDots.sass';

const PureButton = pure(Button);
const Picture = pure('image');

export class CameraDots extends React.Component {
  constructor(props) {
    super(props);
    this.svg = null;
    this.image = new Image();
    this.dragSubscribtion = null;
    this.clampX = identity;
    this.clampY = identity;
    this.makeID = () => Math.random().toString(16).slice(2, 6);
    this.withID = dot => assoc('id', this.makeID(), dot);
    this.withIDs = map(this.withID);
    this.state = {
      isTouched: false,
      width: 0,
      height: 0,
      radius: 8,
      minDots: 4,
      maxDots: 4,
      ratio: 1,
      spaces: '',
      dots: []
    };

    this.image.onload = () => setTimeout(this.onLoadImage, 0);
    this.image.src = this.props.image;

    // has in browser cache
    if (this.image.complete) {
      this.image.removeEventListener('load', this.onLoadImage);
      this.onLoadImage();
    }
  }

  componentWillUnmount() {
    this.image.removeEventListener('load', this.onLoadImage);

    if (this.dragSubscribtion !== null) {
      this.dragSubscribtion.unsubscribe();
    }
  }

  onLoadImage = () => {
    const { naturalHeight, naturalWidth } = this.image;
    const { radius } = this.state;

    const ratio = this.svg.clientWidth / this.image.naturalWidth;
    const offset = 70 * ratio;

    const width = naturalWidth * ratio;
    const height = naturalHeight * ratio;

    const dots = this.withIDs([
      { x: offset, y: offset },
      { x: width - offset, y: offset },
      { x: width - offset, y: height - offset },
      { x: offset, y: height - offset }
    ]);

    this.clampX = clamp(radius, width - radius);
    this.clampY = clamp(radius, height - radius);

    this.setState({ width, height, ratio, dots }, this.subscribeDrag);
  };

  getPolygonPoints() {
    return this.state.dots.reduce((points, dot) => `${points} ${dot.x},${dot.y}`, '').trim();
  }

  setSpaces = (event) => {
    const { value } = event.target;
    this.setState({ spaces: value });
  };

  subscribeDrag = () => {
    /* eslint-disable no-param-reassign */
    const drag$ = fromEvent(this.svg, 'mousedown').pipe(
      filter(event => event.target.nodeName === 'circle'),
      mapObservable(event => event.target),
      flatMap(circle =>
        fromEvent(this.svg, 'mousemove').pipe(
          takeUntil(merge(fromEvent(this.svg, 'mouseup'), fromEvent(this.svg, 'mouseleave'))),
          mapObservable((event) => {
            const { top, left } = this.svg.getBoundingClientRect();
            const { clientX, clientY } = event;
            return {
              id: circle.dataset.id,
              x: this.clampX(Math.ceil(clientX - left)),
              y: this.clampY(Math.ceil(clientY - top))
            };
          }),
          mapObservable(dot => ({
            index: Number(circle.dataset.index),
            dot
          }))
        )
      )
    );

    this.dragSubscribtion = drag$.subscribe(({ index, dot }) => {
      this.setState(state => ({
        isTouched: true,
        dots: update(index, dot, state.dots)
      }));
    });
  };

  done = (event) => {
    event.preventDefault();

    if (!this.state.isTouched) {
      this.props.onAlert({
        status: 'danger',
        message: 'Вы не разметили зону парковки, система не будет работать',
        timeout: 2000
      });

      return;
    }

    const { ratio, spaces } = this.state;
    this.props.onDone({
      spaces: Number(spaces.trim()),
      dots: this.state.dots.map(dot => ({
        x: Math.ceil(dot.x / ratio),
        y: Math.ceil(dot.y / ratio)
      }))
    });
  };

  handleDoubleClick = (event) => {
    const el = event.target;

    if (el.nodeName === 'circle') {
      this.removeDot(Number(el.dataset.index));
      return;
    }

    const { top, left } = this.svg.getBoundingClientRect();
    const { clientX, clientY } = event;
    const dot = {
      x: clientX - left,
      y: clientY - top
    };

    this.addDot(dot);
  };

  addDot = (dot) => {
    if (this.state.dots.length >= this.state.maxDots) {
      return;
    }

    this.setState(state => ({
      dots: [...state.dots, this.withID(dot)]
    }));
  };

  removeDot = (index) => {
    if (this.state.dots.length === this.state.minDots) {
      return;
    }

    this.setState(state => ({
      dots: [...state.dots.slice(0, index), ...state.dots.slice(index + 1)]
    }));
  };

  toggleExample = (event) => {
    event.preventDefault();
    this.setState(state => ({
      showExample: !state.showExample
    }));
  };

  render() {
    const { width, height } = this.state;

    return (
      <Form className="camera-dots" onSubmit={this.done}>
        <div className="camera-dots__note">
          Переместите точки, чтобы обозначить границы парковки. <br />
          Обведите только один ряд автомобилей (ограничение демо-версии)
        </div>

        <button className="camera-dots__toggle-link" onClick={this.toggleExample}>
          {this.state.showExample
            ? 'Скрыть пример'
            : 'Показать пример'
          }
        </button>

        {this.state.showExample && (
          <img className="camera-dots__example" src={example} alt="Пример" />
        )}

        <Gutter size={1.5} />

        <Form.Field className="camera-dots__spaces" label="Количество мест">
          <TextInput
            className="camera-dots__input"
            value={this.state.spaces}
            onChange={this.setSpaces}
            as={MaskedInput}
            mask="111"
            placeholderChar=" "
            required
          />
        </Form.Field>
        <Gutter size={0.5} />
        <Form.Note>
          Укажите количество автомобилей, которое может поместиться в размеченной области
        </Form.Note>

        <Gutter size={1} />

        <svg
          width="100%"
          height={height}
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
          className="camera-dots__svg"
          onDoubleClick={this.handleDoubleClick}
          ref={(ref) => {
            this.svg = ref;
          }}
        >
          <Picture
            width={width}
            height={height}
            xlinkHref={this.props.image}
            className="camera-dots__image"
            alt="Изображение с камеры"
          />
          <polygon className="camera-dots__polygon" points={this.getPolygonPoints()} />
          {this.state.dots.map((dot, index) => (
            <circle
              data-id={dot.id}
              data-index={index}
              className="camera-dots__dot"
              key={dot.id}
              cx={dot.x}
              cy={dot.y}
              r={this.state.radius}
            />
          ))}
        </svg>
        <Gutter size={1} />
        {this.props.isPending
          ? <Spinner absolute={false} />
          : (
            <PureButton className="camera-dots__button">
              Готово
            </PureButton>
          )
        }
      </Form>
    );
  }
}
