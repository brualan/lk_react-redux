import React from 'react';
import { connect } from 'react-redux';
import DelteIcon from 'react-icons/lib/md/delete';
import { Link } from 'react-router-dom';
import { Chip } from 'core/components/Chip';
import { Card } from 'core/components/Card';
import { Gutter } from 'core/components/Gutter';
import { Spinner } from 'core/components/Spinner';
import { getUser } from 'features/auth/selectors';
import { getCameras, getViewers, getPendingFor } from '../selectors';
import { removeViewer } from '../actions';
import { UserIcon } from './UserIcon';
import './ViewersList.sass';

class ViewersList extends React.Component {
  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  render() {
    return (
      <Card>
        <Card.Title>Пользователи</Card.Title>
        <Card.Body>
          <small>
            В бесплатной демо-версии вы можете дать доступ
            <nobr> 15-ти</nobr> пользователям максимум (по их номеру телефона).
          </small>

          <Gutter size={1} />

          <Chip icon={UserIcon}>
            {this.props.user.phone}
          </Chip>

          <Gutter size={1} />

          {this.props.cameras.length > 0 && this.props.viewers.map(user => (
            <Chip icon={UserIcon} key={user.id} className="viewers-list__chip">
              {user.phone}
              <button
                onClick={() => this.props.onRemove(user.pivot)}
                className="viewers-list__remove"
                type="button"
              >
                <DelteIcon />
              </button>
            </Chip>
          ))}

          <Gutter size={1} />

          {this.props.cameras.length > 0 && this.props.viewers.length < 15 && (
            <Chip
              as={Link}
              icon={UserIcon}
              className="viewers-list__add"
              to={`/cameras/${this.props.cameras[0].id}/viewer`}
            >
              Добавить пользователя
            </Chip>
          )}

          <Gutter size={1} />

          {this.props.cameras.length === 0 && (
            <Card.Note status="warning">
              Вы не можете добавить пользователя, если у вас нет подключенных камер
            </Card.Note>
          )}

          {this.props.isPending && (
            <Spinner withOverlay />
          )}
        </Card.Body>
      </Card>
    );
  }
}

const withStore = connect(
  state => ({
    cameras: getCameras(state),
    viewers: getViewers(state),
    isPending: getPendingFor(state, 'viewers'),
    user: getUser(state)
  }), {
    onRemove: removeViewer
  }
);

export default withStore(ViewersList);
