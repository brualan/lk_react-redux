import React from 'react';
import { Card } from 'core/components/Card';
import { Gutter } from 'core/components/Gutter';
import { CamerasList } from './CamerasList';

export const CamerasView = props => (
  <div className="col-md-6">
    <Card>
      <Card.Title>Мои камеры</Card.Title>
      <Card.Body>
        <small>
          В бесплатной демо-версии вы можете зерегистрировать одну камеру
          с вашей парковки и оценить работу сервиса на демо-парковках.
        </small>
        <Gutter size={1} />
        <CamerasList
          isPending={props.isPending}
          cameras={props.cameras}
          onDelete={props.onDelete}
        />
      </Card.Body>
    </Card>
  </div>
);
