import React from 'react';
import { connect } from 'react-redux';
import { Autosuggest } from 'core/components/Autosuggest';
import { Spinner } from 'core/components/Spinner';
import { updateCamera, searchManufacturer, searchModel } from '../actions';
import { getManufacturers, getModels, getPendingFor } from '../selectors';

class CameraManufacturerContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ''
    };
  }

  componentDidMount() {
    this.search({ value: this.state.search });
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      this.search();
    }
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.search(value);
    this.setState({ [name]: value });
  };

  search = ({ value } = {}) => {
    const manufacturer_id = this.props.match.params.id;

    if (!manufacturer_id) {
      this.props.onSearchManufacturer({ value });
    } else {
      this.props.onSearchModel({
        value,
        manufacturer_id
      });
    }
  }

  selectManufacturer = (event, { suggestion }) => {
    const { camera_id } = this.props.match.params;

    this.setState({ search: '' });
    this.props.history.push({
      pathname: `/cameras/${camera_id}/manufacturer/${suggestion.id}`
    });
  };

  selectModel = (event, { suggestion }) => {
    this.props.onSave({
      camera_id: this.props.match.params.camera_id,
      camera_model_id: suggestion.id
    });
  }

  render() {
    const hasManufacturer = !!this.props.match.params.id;
    const inputProps = {
      name: 'search',
      placeholder: 'Поиск...',
      autoFocus: true,
      required: true,
      onChange: this.handleChange,
      value: this.state.search
    };

    return (
      <div className="container">
        <div className="col-md-offset-3 col-md-6">
          {!hasManufacturer ? (
            <h2 className="form-signin-heading">Укажите производителя своей камеры</h2>
          ) : (
            <h2 className="form-signin-heading">Укажите модель своей камеры</h2>
          )}

          {hasManufacturer
            ? (
              <Autosuggest
                alwaysRenderSuggestions
                suggestions={this.props.models}
                getSuggestionValue={s => s.id}
                onSuggestionSelected={this.selectModel}
                onSuggestionsFetchRequested={this.search}
                inputProps={inputProps}
              />
            ) : (
              <Autosuggest
                alwaysRenderSuggestions
                suggestions={this.props.manufacturers}
                getSuggestionValue={s => s.id}
                onSuggestionSelected={this.selectManufacturer}
                onSuggestionsFetchRequested={this.search}
                inputProps={inputProps}
              />
            )
          }
          {this.props.isPending && (
            <Spinner withOverlay />
          )}
        </div>
      </div>
    );
  }
}


const withStore = connect(
  state => ({
    models: getModels(state),
    manufacturers: getManufacturers(state),
    isPending: getPendingFor(state, 'camera')
  }), {
    onSave: updateCamera,
    onSearchModel: searchModel,
    onSearchManufacturer: searchManufacturer
  }
);

export default withStore(CameraManufacturerContainer);
