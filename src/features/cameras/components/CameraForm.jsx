import React from 'react';
import { Form } from 'core/components/Form';
import { TextInput } from 'core/components/TextInput';
import { Gutter } from 'core/components/Gutter';
import { Button } from 'core/components/Button';
import { Spinner } from 'core/components/Spinner';
import './CameraForm.sass';

export const CameraForm = props => (
  <Form onSubmit={props.onSubmit} className="camera-form">
    <Form.Title>
      Добавить камеру
    </Form.Title>
    <Form.Field label="Имя вашей камеры">
      <TextInput
        name="name"
        placeholder="Имя камеры"
        value={props.values.name}
        onChange={props.onChange}
        autoFocus
        required
      />
    </Form.Field>

    <Gutter size={0.7} />

    <Form.Field label="Сетевой адрес камеры (IP)">
      <TextInput
        name="address"
        placeholder="000.000.000.000"
        value={props.values.address}
        onChange={props.onChange}
        required
      />
    </Form.Field>

    <Gutter size={0.7} />

    <Form.Field label="Логин веб-интерфейса камеры">
      <TextInput
        name="login"
        className="form-control"
        placeholder="Логин"
        value={props.values.login}
        onChange={props.onChange}
        required
      />
    </Form.Field>

    <Gutter size={0.7} />

    <Form.Field label="Пароль камеры">
      <TextInput
        name="password"
        placeholder="******"
        value={props.values.password}
        onChange={props.onChange}
        type="password"
        required
      />
    </Form.Field>

    <Gutter size={0.7} />

    {props.isPending
      ? <Spinner absolute={false} />
      : (
        <Button type="submit">
          Добавить
        </Button>
      )
    }
  </Form>
);
