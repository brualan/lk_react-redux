import React from 'react';
import { connect } from 'react-redux';
import { fetchCameras, deleteCamera } from '../actions';
import { getCameras, getPendingFor } from '../selectors';
import { CamerasView } from './CamerasView';

class CamerasContainer extends React.Component {
  componentDidMount() {
    this.props.onFetch();
  }

  render() {
    return (
      <CamerasView
        isPending={this.props.isPending}
        cameras={this.props.cameras}
        onDelete={this.props.onDelete}
      />
    );
  }
}


const withStore = connect(
  state => ({
    cameras: getCameras(state),
    isPending: getPendingFor(state, 'cameras')
  }), {
    onFetch: fetchCameras,
    onDelete: deleteCamera
  }
);

export default withStore(CamerasContainer);
