import React from 'react';

export const UserIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="18" viewBox="0 0 16 18">
    <g fill="none" fillRule="evenodd">
      <path d="M-16-15h48v48h-48z" />
      <g fill="#69C215" fillRule="nonzero">
        <path d="M8 10c-4.411 0-8 3.452-8 7.694 0 .169.148.306.33.306h15.34c.182 0 .33-.137.33-.306C16 13.452 12.411 10 8 10zM8 8c2.205 0 4-1.794 4-4s-1.794-4-4-4-4 1.794-4 4 1.794 4 4 4z" />
      </g>
    </g>
  </svg>
);
