import React from 'react';
import { connect } from 'react-redux';
import { Spinner } from 'core/components/Spinner';
import * as alerts from 'features/alerts';
import { CameraDots } from './CameraDots';
import { fetchImage, saveDots } from '../actions';
import { getCameraById, getPendingFor } from '../selectors';

class CameraImageContainer extends React.Component {
  componentDidMount() {
    this.props.onFetch(this.props.match.params.id);
  }

  canEdit() {
    const { camera } = this.props;

    return camera === undefined || (
      camera.image_url === undefined ||
      camera.image_url === null
    );
  }

  saveDots =({ spaces, dots }) => {
    this.props.onSaveDots({
      camera_id: this.props.match.params.id,
      parking_spaces_count: spaces,
      dots: dots.map(dot => ({
        x_coord: dot.x,
        y_coord: dot.y
      }))
    });
  }

  render() {
    if (this.props.isPending) {
      return (
        <div style={{ marginTop: 60 }}>
          <Spinner withOverlay />
        </div>
      );
    }

    if (this.props.camera === undefined || !this.props.camera.image_url) {
      return (
        <div style={{ marginTop: 60 }}>
          <Spinner withOverlay />
        </div>
      );
    }

    return (
      <CameraDots
        image={this.props.camera.image_url}
        isPending={this.props.isDotsPending}
        onAlert={this.props.onAlert}
        onDone={this.saveDots}
      />
    );
  }
}

const withStore = connect(
  (state, props) => ({
    camera: getCameraById(state, props.match.params.id),
    isImagePending: getPendingFor(state, 'image'),
    isDotsPending: getPendingFor(state, 'dots')
  }), {
    onFetch: fetchImage,
    onAlert: alerts.actions.add,
    onSaveDots: saveDots
  }
);

export default withStore(CameraImageContainer);
