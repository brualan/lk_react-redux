import { ADD, REMOVE } from './constants';

export const add = ({ status, message, timeout = Infinity }) => ({
  type: ADD,
  payload: {
    id: Math.random().toString(16).slice(2, 8),
    status,
    message,
    timeout
  }
});

export const remove = id => ({
  type: REMOVE,
  payload: {
    id
  }
});
