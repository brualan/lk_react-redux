import React from 'react';
import './Alert.sass';

export const Alert = ({ id, status, message, onRemove }) => (
  <div className={`alert alert_${status}`} role="alert">
    <span>{message}</span>
    <button type="button" className="alert__close" onClick={() => onRemove(id)}>
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
);
