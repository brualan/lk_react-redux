import React from 'react';
import { Alert } from './Alert';
import './Alerts.sass';

export const Alerts = ({ alerts, onRemove }) => (
  <div className="alerts">
    {alerts.map(alert => (
      <Alert
        key={alert.id}
        id={alert.id}
        status={alert.status}
        message={alert.message}
        onRemove={onRemove}
      />
    ))}
  </div>
);
