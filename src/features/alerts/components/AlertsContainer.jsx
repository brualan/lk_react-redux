import { connect } from 'react-redux';
import { remove } from '../actions';
import { Alerts } from './Alerts';

const withStore = connect(
  state => ({ alerts: state.alerts }), {
    onRemove: remove
  }
);

export default withStore(Alerts);
