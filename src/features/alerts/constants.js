import { createType, withNamespace } from 'store/utils';

export const NAMESPACE = 'ALERTS';

const type = withNamespace(NAMESPACE, createType);

export const ADD = type('ADD');
export const REMOVE = type('REMOVE');
export const CLEAR_ALL = type('CLEAR_ALL');

export const statuses = {
  INFO: 'info',
  SUCCESS: 'success',
  WARNING: 'warning',
  ERROR: 'danger'
};
