import { ofType } from 'redux-observable';
import { Observable } from 'rxjs/Observable';
import { timer } from 'rxjs/observable/timer';
import { mapTo, flatMap, filter, takeUntil } from 'rxjs/operators';
import { remove } from './actions';
import { ADD, REMOVE } from './constants';


export function alertsEpic(action$) {
  return action$.pipe(
    ofType(ADD),
    flatMap(({ payload: { id, timeout } }) => new Observable((observer) => {
      if (timeout === Infinity) {
        observer.complete();
        return;
      }

      const alreadyRemoved$ = action$.pipe(
        ofType(REMOVE), filter(action => action.payload.id === id)
      );

      const remove$ = timer(timeout).pipe(
        takeUntil(alreadyRemoved$),
        mapTo(remove(id))
      );

      const unsub = remove$.subscribe((action) => {
        observer.next(action);
        observer.complete();
      });

      observer.add(unsub);
    }))
  );
}
