import { ADD, REMOVE } from './constants';

export default function alerts(state = [], action) {
  switch (action.type) {
  case ADD:
    return [...state, action.payload];
  case REMOVE:
    return state.filter(alert => alert.id !== action.payload.id);
  default:
    return state;
  }
}
