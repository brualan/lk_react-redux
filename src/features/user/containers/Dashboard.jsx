import React from 'react';

import CamerasContainer from 'features/cameras/components/CamerasContainer';
import ViewersList from '../../viewers/components/ViewersList';

// implements a panel with cameras and viewers
const Dashboard = () => (
  <div className="container">
    <div className="row">
      <CamerasContainer />
      <ViewersList />
    </div>
  </div>
);

export default Dashboard;
