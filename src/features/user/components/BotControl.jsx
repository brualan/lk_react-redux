import React from 'react';
import { Link } from 'react-router-dom';

const BotControl = () => (
  <div className="container">
    <div className="col-md-offset-3 col-md-6">
      <a target="_blank" href="https://telegram.me/etoParkingBot" rel="noopener noreferrer">
        <button className="btn btn-lg btn-primary btn-block">
          Добавить Telegram-бота
        </button>
      </a>
      <p>После добавления бота у вас появится доступ к демо-камере.</p>
      <p>Если бот добавлен успешно, вы можете перейти к управлению камерами</p>
      <Link to="/" className="btn btn-lg btn-borring btn-block">
        Перейти к камерам
      </Link>
      <p>
        Если у вас нет приложения Telegram,
        скачайте его на телефон (в AppStore или Google Play) или компьютер.
      </p>
    </div>
  </div>
);

export default BotControl;
