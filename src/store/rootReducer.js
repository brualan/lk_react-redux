import { combineReducers } from 'redux';

import * as auth from 'features/auth';
import { reducer as alerts } from 'features/alerts';
import { reducers as cameras } from 'features/cameras';

export const rootReducer = combineReducers({
  authentication: auth.reducer,
  entities: combineReducers({
    cameras: cameras.entities.cameras,
    viewers: cameras.entities.viewers
  }),
  ui: combineReducers({
    cameras: combineReducers({
      pendings: cameras.ui.pendings,
      manufacturers: cameras.ui.manufacturers,
      models: cameras.ui.models
    })
  }),
  alerts
});
