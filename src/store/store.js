import axios from 'axios';
import { applyMiddleware, compose, createStore } from 'redux';
import { Observable } from 'rxjs/Observable';
import { createEpicMiddleware } from 'redux-observable';
import thunkMiddleware from 'redux-thunk';
import { api } from 'core/api';
import { storage, history } from 'core/browser';

import { rootReducer } from './rootReducer';
import { rootEpic } from './rootEpic';

const epicMiddleware = createEpicMiddleware(rootEpic, {
  dependencies: {
    history,
    storage,
    fromApi(request) {
      return new Observable((observer) => {
        const source = axios.CancelToken.source();

        observer.add(() => source.cancel('Operation canceled by the user.'));

        api({ ...request, cancelToken: source.token })
          .then(response => observer.next(response))
          .catch((reason) => {
            if (!axios.isCancel(reason)) {
              observer.error(reason);
            }
          });
      });
    }
  }
});

const middlewares = applyMiddleware(
  thunkMiddleware,
  epicMiddleware
);

const enhancer = compose(
  middlewares,
  window.devToolsExtension ? window.devToolsExtension() : f => f
);

export const store = createStore(rootReducer, {}, enhancer);

if (module.hot) {
  /* eslint-disable global-require */

  module.hot.accept('./rootReducer', () => {
    const { rootReducer: updatedReducer } = require('./rootReducer');
    store.replaceReducer(updatedReducer);
  });

  module.hot.accept('./rootEpic', () => {
    const { rootEpic: updatedEpic } = require('./rootEpic');
    epicMiddleware.replaceEpic(updatedEpic);
  });
}
