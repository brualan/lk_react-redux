import { combineEpics } from 'redux-observable';
import { epic as alertsEpic } from 'features/alerts';
import { epic as authEpic } from 'features/auth';
import { epic as camerasEpic } from 'features/cameras';

export const rootEpic = combineEpics(
  authEpic,
  alertsEpic,
  camerasEpic
);

