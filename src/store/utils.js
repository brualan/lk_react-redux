export const createType = type => type.toUpperCase();
export const createAsyncType = (type) => {
  const BASE = createType(type);

  return {
    BASE,
    REQUEST: `${BASE}_REQUEST`,
    SUCCESS: `${BASE}_SUCCESS`,
    FAILURE: `${BASE}_FAILURE`,
    CANCELLED: `${BASE}_CANCELLED`
  };
};

export const withNamespace = (ns, creator = createType) => type => creator(`[${ns}] ${type}`);
