import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { store } from 'store';
import App from './App';

/* Root component for HMR support */
export const Root = ({ history }) => (
  <Provider store={store}>
    <Router basename="/lk" history={history}>
      <App />
    </Router>
  </Provider>
);
